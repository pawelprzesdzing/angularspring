import {EmployeeDetailsComponent} from './employee-details/employee-details.component';
import {CreateEmployeeComponent} from './create-employee/create-employee.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {UpdateEmployeeComponent} from './update-employee/update-employee.component';
import {HomeComponent} from "./home/home.component";
import {AdministratorPanelComponent} from "./administrator-panel/administrator-panel.component";
import {AuthGuard} from "./helpers/auth.guard";
import {LoginComponent} from "./login/login.component";
import {FixedAssetsComponent} from "./fixed assets/fixed-assets.component";

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent },
  { path: 'employees', component: EmployeeListComponent },
  { path: 'add', component: CreateEmployeeComponent },
  { path: 'update/:id', component: UpdateEmployeeComponent },
  { path: 'details/:id', component: EmployeeDetailsComponent },
  { path: 'admin_panel', component: AdministratorPanelComponent },
  { path: 'login', component: LoginComponent },
  { path: 'fixed_assets', component: FixedAssetsComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
