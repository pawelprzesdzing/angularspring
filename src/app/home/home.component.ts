import {Component, OnInit} from "@angular/core";
import {EmployeeService} from "../employee.service";
import {Router} from "@angular/router";
import {User} from "../model";
import {UserService} from "../service";
import {first} from "rxjs/operators";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent {
  loading = false;
  users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loading = true;
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.loading = false;
      this.users = users;
    });
  }
}
